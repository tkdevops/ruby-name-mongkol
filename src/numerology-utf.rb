﻿#encoding: UTF-8

## ****************************************************************************
## Ruby
## Project name : Numerology
## Objective    : ตรวจชื่อตามหลักเลขศาสตร์ พร้อมแสดงดาว ตามหลักเลขศาสตร์ 
##                จากหนังสือ นามสกุลหนุนดวงชะตา จุฑามาศ ณ สงขลา
## start date   : 2 ม.ค. 2552
## programmer   : bennueng
## email        : bennueng@gmail.com
## ****************************************************************************


class Numerology
    # ข้อกำหนด ดาวเคราะห์ 1 - 9
    NumerologyTable = 
    [   "1 ก ด  ุ่ ถ ภ า ฤ ท ำ A I J Q Y ",
        "2ขชงูเบแป้BKR",
        "3ฆตฑฒ๋CGLS",
        "4คธญัริษะโDMT",
        "5ฉฌณนมหฎฮฬึEHNX",
        "6จลวอใUVW",
        "7ซีศืส๊OZ",
        "8ผฝพฟย็FP",
        "9ไฏฐ์"
    ]


    # ตรวจดูว่า อักษรนั้น อยู่ใน ดาวเคราะห์อะไร
    # input :
    #       char = ตัวอักษร
    # output:
    #       ค่าดาวที่เทียบแล้วว่าอักษรนั้นเป็นดาวอะไร (คืนค่า 0 ถ้าไม่มี)
    def get_star(char)
        return 0 if char.length <= 0
        star = 0
        while (star < NumerologyTable.length)
            return (star + 1) if NumerologyTable[star].include? char.split(//)[0].upcase
            star += 1
        end
        return 0
    end

    # แสดงผลรวมเลขศาสตร์ จากชื่อ
    # input  : 
    #       name = ชื่อที่ต้องการตรวจสอบ
    #       show_star == true  จะแสดงดาวที่บวกออกมาเป็นเลขศาสตร์ด้วย
    # output :
    #       ส่งกลับค่าเลขศาสตร์ ซึ่งมาจากดาวแต่ละดวงเอามาบวกกัน    
    def get_numerology(name, show_star = false)
        soul_number = 0
        name.split(//).each do |c|
            star = get_star(c)
            print "> #{c}(#{star})" if show_star
            soul_number += star
        end
        return soul_number
    end

	# คืนค่าเลขเหยียด 2 หลัก เช่น 12 = 1+2 = 3
    # ถ้าต้องการมากกว่า 2 หลัก ก็ให้กลับมาเรียกอีกครั้ง
    # input :
    #   number = ค่าตัวเลขที่ต้องการทำเลขเหยียด
    # output:
    #   คืนค่าเลขที่บวกกันแบบเลขเหยียด
    def each_plus(number)
        result = 0
        number.to_s.split(//).each do |x| result += x.to_i end
        return result
    end

end

# main ====================================================================
number = Numerology.new

=begin
# ส่วนตรวจสอบการทำงานของ เลขเหยียด
100.step(220,20) do |x|
    puts "#{x} = #{number.each_plus(x)}"
end
=end



# main โปรแกรมหลักของโปรแรกมตรวจชื่อ
puts "******************************************************************"
puts "ตรวจชื่อตามหลักเลขศาสตร์ พร้อมแสดงดาว ตามหลักเลขศาสตร์"
puts "จากหนังสือ นามสกุลหนุนดวงชะตา จุฑามาศ ณ สงขลา"
puts "******************************************************************"
puts "พิมพ์ชื่อที่ต้องการตรวจสอบ.."

# 1. รับค่าจาก Command line
#puts " = #{number.get_numerology(ARGV[0],true)}"

# 2. ตัวอย่าง 
#name = "ขจรฤทธิ์"
#puts " = #{number.get_numerology(name,true)}"

# 3. โปรแกรมปกติ
while (name = gets.chop!) != "exit"
    puts " = #{number.get_numerology(name,true)}"
end


=begin
# ส่วนการโหลดไฟล์ไปตรวจสอบเพื่อหาชื่อที่มีเลขศาสตร์ลง 45
File.open("999 Surname 2.txt","r") do |file|
    count = 0
    while line = file.gets
        surname = line.split 
         # เพราะการแบ่ง 1 บรรทัดจะได้ 4 ส่วน จึงเอาเฉพาะนามสกุล
        next if surname[0] == nil
        if number.get_numerology(surname[0],false) == 45
            puts "#{surname[0]}\t"
            puts "#{number.get_numerology(surname[0],true)}"
            puts "อ่านว่า..#{surname[1]}\t\t"
            puts "มาจาก..#{surname[2]}\t\t"
            puts "แปลว่า..#{surname[3]}\t\t"
            puts "============================================"
            count += 1
        end
    end
    puts "#{count}.. records"
end
=end
